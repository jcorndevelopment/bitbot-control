package at.petritzdesigns.bitbot.controller.exceptions;

import at.petritzdesigns.bitbot.exceptions.BitBotException;

/**
 * All exceptions that are thrown in the graphical user interface should be of
 * this type.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BitBotGuiException extends BitBotException {

    /**
     * Empty Constructor
     */
    public BitBotGuiException() {
    }

    /**
     * Constructor with message
     *
     * @param msg the message
     */
    public BitBotGuiException(String msg) {
        super(msg);
    }
}
