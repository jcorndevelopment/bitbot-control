package at.petritzdesigns.bitbot.controller;

import at.petritzdesigns.bitbot.controller.view.MainFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * The class that is executed first. It loads the main frame.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BitBot {

    /**
     * Main entry point
     *
     * @param args arguments<br> {@code -server} to start server<br>
     * nothing to start gui
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    MainFrame mf = new MainFrame();
                    mf.setVisible(true);
                }
            });
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

}
