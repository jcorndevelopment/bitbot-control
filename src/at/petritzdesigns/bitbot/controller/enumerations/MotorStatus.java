package at.petritzdesigns.bitbot.controller.enumerations;

/**
 * The current status of the motor.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public enum MotorStatus {
    /**
     * Motor should run backwards
     */
    BACKWARDS,
    /**
     * Motor should run forwards
     */
    FORWARDS,
    /**
     * Motor should stop
     */
    STOP;
}
