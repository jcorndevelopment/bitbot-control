package at.petritzdesigns.bitbot.controller.utility;

/**
 * For every configuration item, there is a display type. This determines the
 * type of the config value.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public enum DisplayType {
    /**
     * Opens a SaveFileDialog
     */
    SAVE_FILE,
    /**
     * Opens a OpenFileDialog
     */
    OPEN_FILE,
    /**
     * Just a normal text field
     */
    INPUT_TEXT
}
