package at.petritzdesigns.bitbot.controller.utility;

import at.petritzdesigns.bitbot.exceptions.BitBotServerException;
import java.awt.Component;
import javax.swing.JOptionPane;

/**
 * Logging class suitable for logging in the graphical user interface.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Logger {

    /**
     * Instance needed for use with singletons
     */
    private static Logger instance;

    /**
     * Default Constructor
     */
    private Logger() {
    }

    /**
     * Creates and returns instance
     *
     * @return instance
     */
    public static Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    /**
     * Debug message (short version)
     *
     * @param text to debug
     */
    public static void d(Object text) {
        getInstance().debug(text);
    }

    /**
     * Error message (short version)
     *
     * @param text to debug
     */
    public static void e(Object text) {
        getInstance().error(text);
    }

    /**
     * Warning message (short version)
     *
     * @param text to debug
     */
    public static void w(Object text) {
        getInstance().warning(text);
    }

    /**
     * Message to show in parent (short version)
     *
     * @param parent parent component
     * @param text to debug
     */
    public static void v(Component parent, Object text) {
        getInstance().view(parent, text);
    }

    /**
     * Debug Message (long version)<br>
     * Writes Status
     *
     * @see Status
     * @param text to debug
     */
    public void debug(Object text) {
        String description = getDescription(text);
        System.out.println("DEBUG: " + description);
        Status.getInstance().write(description);
    }

    /**
     * Error Message (long version)<br>
     * Writes Status
     *
     * @see Status
     * @param text to debug
     */
    public void error(Object text) {
        String description = getDescription(text);
        System.out.println("ERROR: " + description);
        Status.getInstance().write("Error: " + description);
    }

    /**
     * Warning Message (long version)<br>
     * Writes Status
     *
     * @see Status
     * @param text to debug
     */
    public void warning(Object text) {
        String description = getDescription(text);
        System.out.println("WARNING: " + description);
        Status.getInstance().write("Warning: " + description);
    }

    /**
     * Message to show in parent (long version)<br>
     * Writes Status and shows JOptionPane
     *
     * @see Status
     * @param parent parent component
     * @param text to debug
     */
    public void view(Component parent, Object text) {
        String description = getDescription(text);
        System.out.println("VIEW: " + description);
        Status.getInstance().write("View: " + text.toString());
        JOptionPane.showMessageDialog(parent, text.toString());
    }

    /**
     * Get useful description of the object
     *
     * @param obj the object
     * @return description
     */
    public String getDescription(Object obj) {
        if (obj instanceof BitBotServerException) {
            StringBuilder sb = new StringBuilder();
            if (((BitBotServerException) obj).hasInitialException()) {
                sb.append(getExceptionTrace(((BitBotServerException) obj).getInitialException()));
                sb.append("\n");
            }
            sb.append(getExceptionTrace((Exception) obj));
            return sb.toString();
        }
        if (obj instanceof Exception) {
            return getExceptionTrace((Exception) obj);
        }
        return obj.toString();
    }

    /**
     * Returns exception stack trace
     *
     * @param ex the exception
     * @return strack trace
     */
    public String getExceptionTrace(Exception ex) {
        StringBuilder buffer = new StringBuilder();

        int count = Thread.currentThread().getStackTrace().length < 5 ? 0 : 5;
        int length = Thread.currentThread().getStackTrace().length < 10 ? Thread.currentThread().getStackTrace().length : 10;
        for (int i = count; i < length; ++i) {
            if (Thread.currentThread().getStackTrace().length < i) {
                break;
            }
            String fullClassName = Thread.currentThread().getStackTrace()[i].getClassName();
            String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
            String methodName = Thread.currentThread().getStackTrace()[i].getMethodName();
            int lineNumber = Thread.currentThread().getStackTrace()[i].getLineNumber();
            buffer.append(className).append(".").append(methodName).append("():").append(lineNumber).append("\n");
        }
        buffer.append("---------------------------------------------------\t\n").append(ex).append("\n");
        return buffer.toString();
    }
}
