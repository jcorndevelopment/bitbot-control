package at.petritzdesigns.bitbot.controller.utility;

import at.petritzdesigns.bitbot.utility.FileCreator;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Configuration class for the graphical user interface.
 * <br>
 * The configuration is stored in a properties file that is based on key-value
 * pairs.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Config {

    /**
     * Instance needed for use with Singletons
     */
    private static Config instance;

    /**
     * Properties object
     */
    private Properties properties;

    /**
     * Default Constructor<br>
     * creates new Properties object, fills it with default config and reads
     * config items from file
     *
     * @see Properties
     */
    private Config() {
        try {
            properties = new Properties();
            setupDefaultConfig();
            FileCreator.createFileIfNotExist(get("config_path"));
            properties.load(new FileInputStream(get("config_path")));
        } catch (IOException ex) {
            Logger.e(ex);
        } catch (Exception ex) {
            Logger.e(ex);
        }
    }

    /**
     * Creates and returns instance
     *
     * @return instance
     */
    public static Config getDefault() {
        if (instance == null) {
            instance = new Config();
        }
        return instance;
    }

    /**
     * Get Value by given key
     *
     * @param key the Key
     * @return value
     * @see Properties
     */
    public String get(String key) {
        String val = properties.getProperty(key);
        if (val == null) {
            return "";
        }
        return val;
    }

    /**
     * Sets new Property by given key and value<br>
     * Writes the property out in a new thread (async)
     *
     * @param key the Key
     * @param value the Value
     * @return success
     * @see Properties
     */
    public boolean set(String key, String value) {
        try {
            properties.setProperty(key, value);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        properties.store(new FileWriter(get("config_path")), "API Tester");
                    } catch (IOException ex) {
                        Logger.e(ex);
                    } catch (Exception ex) {
                        Logger.e(ex);
                    }
                }
            }).start();
        } catch (Exception ex) {
            Logger.d(ex);
            return false;
        }

        return true;
    }

    /**
     * Get Value by given key
     *
     * @param key the Key
     * @return value as boolean
     * @see Properties
     */
    public boolean getBool(String key) {
        String val = get(key);
        if (val.isEmpty()) {
            return false;
        }
        return val.equals("true");
    }

    /**
     * Sets new Property by given key and value (boolean)<br>
     * Writes the property out in a new thread (async)
     *
     * @param key the Key
     * @param value the Value as boolean
     * @return success
     * @see Properties
     */
    public boolean set(String key, boolean value) {
        return set(key, value + "");
    }

    /**
     * Get Value by given key
     *
     * @param key the Key
     * @return value as Integer
     * @see Properties
     */
    public int getInt(String key) {
        try {
            return Integer.parseInt(get(key));
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    /**
     * Sets new Property by given key and value (Integer)<br>
     * Writes the property out in a new thread (async)
     *
     * @param key the Key
     * @param value the Value as Integer
     * @return success
     * @see Properties
     */
    public boolean set(String key, int value) {
        return set(key, value + "");
    }

    /**
     * Deletes a Property by given key<br>
     * Also writes the properties (except the one to delete) out.
     *
     * @param key the Key
     * @return success
     * @see Properties
     */
    public boolean delete(String key) {
        try {
            properties.remove(key);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        properties.store(new FileWriter(get("config_path")), "API Tester");
                    } catch (IOException ex) {
                        Logger.e(ex);
                    } catch (Exception ex) {
                        Logger.e(ex);
                    }
                }
            }).start();
        } catch (Exception ex) {
            Logger.d(ex);
            return false;
        }

        return true;
    }

    /**
     * Return Config as a Map
     *
     * @return map
     */
    public Map<String, String> getConfigAsMap() {
        Map<String, String> temp = new HashMap<>();
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            temp.put(entry.getKey().toString(), entry.getValue().toString());
        }
        return temp;
    }

    /**
     * Initializes default config
     */
    private void setupDefaultConfig() {
        properties.setProperty(ConfigItem.CONFIG_PATH.getName(), "config/default.pdc");
        properties.setProperty(ConfigItem.DATA_PATH.getName(), "data/default.pdd");
        properties.setProperty(ConfigItem.CONNECTION_TYPE.getName(), "udp");
    }

}
