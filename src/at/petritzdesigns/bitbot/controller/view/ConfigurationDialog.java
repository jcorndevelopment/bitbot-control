package at.petritzdesigns.bitbot.controller.view;

import at.petritzdesigns.bitbot.controller.utility.Config;
import at.petritzdesigns.bitbot.controller.utility.ConfigItem;
import at.petritzdesigns.bitbot.controller.utility.DisplayType;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * The configuration dialog. It creates the fields for editing automatically.
 *
 * @see Config
 * @see ConfigItem
 * @see DisplayType
 * @author Markus Petritz
 * @version 1.0.0
 */
public class ConfigurationDialog extends javax.swing.JDialog {

    /**
     * Map for new configuration items
     */
    private final Map<String, Map.Entry<DisplayType, Component>> newConfig;

    /**
     * Default Constructor<br>
     * creates empty map
     *
     * @param parent parent component
     * @param modal modal
     */
    public ConfigurationDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.newConfig = new HashMap<>();
        initComponents();
    }

    /**
     * Method to add custom items (called from initComponents)<br>
     * reads all config elements and displays them using designated display
     * type.
     *
     * @see Config
     * @see DisplayType
     */
    private void onAddItems() {
        Map<String, String> configItems = Config.getDefault().getConfigAsMap();
        pnMain.setLayout(new GridLayout(configItems.size(), 2));

        for (Map.Entry<String, String> entry : configItems.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            ConfigItem item = ConfigItem.contains(key);
            DisplayType type;
            if (item != null) {
                type = item.getDisplayType();
            } else {
                type = DisplayType.INPUT_TEXT;
            }

            JLabel lbKey = new JLabel(item != null ? item.getDescription() : key);
            lbKey.setFont(new java.awt.Font("Helvetica Neue", 0, 14));
            Component cValue = null;

            switch (type) {
                case INPUT_TEXT:
                    cValue = new JTextField(value);
                    break;
                case OPEN_FILE:
                    cValue = new JTextField(value);
                    cValue.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            JTextField src = (JTextField) e.getSource(); //cannot be something different than JTextField
                            JFileChooser fc = new JFileChooser(src.getText());
                            int ret = fc.showOpenDialog(src.getParent());
                            if (ret == JFileChooser.APPROVE_OPTION) {
                                File f = fc.getSelectedFile();
                                src.setText(f.getAbsolutePath()); //better would be canonical path but i am to lazy for exceptions
                            }
                        }
                    });
                    break;
                case SAVE_FILE:
                    cValue = new JTextField(value);
                    cValue.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            JTextField src = (JTextField) e.getSource(); //cannot be something different than JTextField
                            JFileChooser fc = new JFileChooser(src.getText());
                            int ret = fc.showSaveDialog(src.getParent());
                            if (ret == JFileChooser.APPROVE_OPTION) {
                                File f = fc.getSelectedFile();
                                src.setText(f.getAbsolutePath()); //better would be canonical path but i am to lazy for exceptions
                            }
                        }
                    });
                default:
                    break;
            }

            cValue.setFont(new java.awt.Font("Helvetica Neue", 0, 14));

            newConfig.put(key, new AbstractMap.SimpleEntry<>(type, cValue));
            pnMain.add(lbKey);
            pnMain.add(cValue);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnTitle = new javax.swing.JLabel();
        pnToolbar = new javax.swing.JPanel();
        btSave = new javax.swing.JButton();
        btDiscard = new javax.swing.JButton();
        pnMain = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("API Tester - Configuration");

        pnTitle.setFont(new java.awt.Font("Helvetica Neue", 0, 24)); // NOI18N
        pnTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        pnTitle.setText("Configuration");
        getContentPane().add(pnTitle, java.awt.BorderLayout.PAGE_START);

        pnToolbar.setLayout(new java.awt.GridLayout(1, 0));

        btSave.setText("Save Changes");
        btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onSave(evt);
            }
        });
        pnToolbar.add(btSave);

        btDiscard.setText("Discard Changes");
        btDiscard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onDiscard(evt);
            }
        });
        pnToolbar.add(btDiscard);

        getContentPane().add(pnToolbar, java.awt.BorderLayout.PAGE_END);

        javax.swing.GroupLayout pnMainLayout = new javax.swing.GroupLayout(pnMain);
        pnMain.setLayout(pnMainLayout);
        pnMainLayout.setHorizontalGroup(
            pnMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 713, Short.MAX_VALUE)
        );
        pnMainLayout.setVerticalGroup(
            pnMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 337, Short.MAX_VALUE)
        );

        onAddItems();
        getContentPane().add(pnMain, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Action Event that is called when the user presses the "Save Changes"
     * button<br>
     * Saves all items from the newConfig map, then closes Dialog.
     *
     * @param evt
     */
    private void onSave(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onSave
        for (Map.Entry<String, Map.Entry<DisplayType, Component>> entry : newConfig.entrySet()) {
            String value;
            switch (entry.getValue().getKey()) {
                case INPUT_TEXT:
                case OPEN_FILE:
                case SAVE_FILE:
                    value = ((JTextField) entry.getValue().getValue()).getText();
                    break;
                default:
                    value = "";
                    break;
            }
            Config.getDefault().set(entry.getKey(), value);
        }

        this.dispose();
    }//GEN-LAST:event_onSave

    /**
     * Action Event that is called when the user presses "Discard Changes"
     * button<br>
     * Closes Dialog.
     *
     * @param evt
     */
    private void onDiscard(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onDiscard
        this.dispose();
    }//GEN-LAST:event_onDiscard

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btDiscard;
    private javax.swing.JButton btSave;
    private javax.swing.JPanel pnMain;
    private javax.swing.JLabel pnTitle;
    private javax.swing.JPanel pnToolbar;
    // End of variables declaration//GEN-END:variables

}
