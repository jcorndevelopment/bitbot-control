package at.petritzdesigns.bitbot.controller.view;

import at.petritzdesigns.bitbot.controller.enumerations.MotorStatus;
import at.petritzdesigns.bitbot.controller.exceptions.BitBotGuiException;
import at.petritzdesigns.bitbot.exceptions.BitBotException;
import at.petritzdesigns.bitbot.exceptions.BitBotServerException;
import at.petritzdesigns.bitbot.networking.BitBotClient;
import at.petritzdesigns.bitbot.networking.BitBotTcpClient;
import at.petritzdesigns.bitbot.networking.BitBotUdpClient;
import at.petritzdesigns.bitbot.networking.data.CommandData;
import at.petritzdesigns.bitbot.networking.data.CommandDataFactory;
import at.petritzdesigns.bitbot.networking.data.responses.Response;
import at.petritzdesigns.bitbot.networking.data.commands.ControlCameraCommand;
import at.petritzdesigns.bitbot.networking.data.commands.ControlMotorCommand;
import at.petritzdesigns.bitbot.networking.data.commands.DistanceCommand;
import at.petritzdesigns.bitbot.networking.data.commands.EstablishConnectionCommand;
import at.petritzdesigns.bitbot.networking.data.responses.DistanceResponse;
import at.petritzdesigns.bitbot.networking.data.responses.FailureResponse;
//import at.petritzdesigns.bitbot.simulation.view.SimulationFrame;
import at.petritzdesigns.bitbot.controller.utility.Config;
import at.petritzdesigns.bitbot.controller.utility.ConfigItem;
import at.petritzdesigns.bitbot.utility.FileCreator;
import at.petritzdesigns.bitbot.utility.Ipv4AddressHelper;
import at.petritzdesigns.bitbot.controller.utility.Logger;
import at.petritzdesigns.bitbot.controller.utility.Status;
import java.awt.HeadlessException;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;

/**
 * The main window. It shows everything that is needed to control the roboter,
 * the camera and the sensors.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * True if udp is enabled
     */
    private boolean udpMode;

    /**
     * Client, where the commands should be send to
     */
    private BitBotClient client;

    /**
     * Simulation window
     */
    //private SimulationFrame simulation;
    /**
     * Configuration Dialog
     */
    private ConfigurationDialog configuration;

    /**
     * Command Data Factory
     */
    private CommandDataFactory dataFactory;

    /**
     * Status of the right motor<br>
     */
    private MotorStatus statusRight;

    /**
     * Status of the left motor<br>
     */
    private MotorStatus statusLeft;

    /**
     * The speed that is added when the speed+ and speed- keys are pressed.
     */
    private final int speedBooster = 5;

    /**
     * Timer for ultra sonic sensor auto refresh
     */
    private Timer ultraSonicSensorTimer;

    /**
     * Default Constructor
     */
    public MainFrame() {
        initComponents();
        initView();
        initMotors();
        initListeners();
    }

    /**
     * Initialize view components
     */
    private void initView() {
        Status.getInstance().setLabel(lbStatus);

        tfIpAddress.requestFocus();
        btConnectToServer.setEnabled(false);
        btDisconnect.setEnabled(false);
        btPanic.setEnabled(false);

        try {
            readIpAdress();
        } catch (IOException ex) {
            Logger.w(ex);
        }
    }

    /**
     * Initialize status of the motors
     */
    private void initMotors() {
        statusRight = MotorStatus.STOP;
        statusLeft = MotorStatus.STOP;
        gmLeft.setCurrentValue(0);
        gmRight.setCurrentValue(0);
    }

    /**
     * Add the gauge value changed listeners
     */
    private void initListeners() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
            @Override
            public boolean dispatchKeyEvent(KeyEvent e) {
                if (e.getID() == KeyEvent.KEY_PRESSED) {
                    switch (e.getKeyCode()) {
                        case KeyEvent.VK_UP:
                            onForward(null);
                            break;
                        case KeyEvent.VK_DOWN:
                            onBackward(null);
                            break;
                        case KeyEvent.VK_LEFT:
                            onLeft(null);
                            break;
                        case KeyEvent.VK_RIGHT:
                            onRight(null);
                            break;
                        case KeyEvent.VK_SPACE:
                            onStop(null);
                            break;
                        case KeyEvent.VK_P:
                            onPanic(null);
                            break;
                        case KeyEvent.VK_X:
                            gmLeft.setCurrentValue(gmLeft.getCurrentValue() + speedBooster);
                            gmRight.setCurrentValue(gmRight.getCurrentValue() + speedBooster);
                             {
                                try {
                                    updateMotors();
                                } catch (BitBotException ex) {
                                    JOptionPane.showMessageDialog(null, ex);
                                }
                            }
                            break;

                        case KeyEvent.VK_Y:
                            gmLeft.setCurrentValue(gmLeft.getCurrentValue() - speedBooster);
                            gmRight.setCurrentValue(gmRight.getCurrentValue() - speedBooster);
                             {
                                try {
                                    updateMotors();
                                } catch (BitBotException ex) {
                                    JOptionPane.showMessageDialog(null, ex);
                                }
                            }
                            break;
                    }
                    return false;
                }
                return false;
            }
        });

        gmLeft.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                try {
                    updateMotors();
                } catch (Exception ex) {
                    Logger.v(getParent(), ex);
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        gmRight.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                try {
                    updateMotors();
                } catch (Exception ex) {
                    Logger.v(getParent(), ex);
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
    }

    /**
     * Listens to simulator server with simulator port
     */
    private void listenToSimulator() throws BitBotException {
        stopListening();
        client = new BitBotUdpClient();
        client.connectToServer();
        dataFactory = new CommandDataFactory();
        sendCommand(EstablishConnectionCommand.class);
        Status.getInstance().write("Started connection to simulator");
    }

    /**
     * Listens to server with specified ip and default port
     *
     * @param ip ip adress
     */
    private void listenToServer(String ip) throws BitBotException {
        stopListening();
        if (udpMode) {
            client = new BitBotUdpClient(ip);
        } else {
            client = new BitBotTcpClient(ip);
        }
        client.connectToServer();

        dataFactory = new CommandDataFactory();
        sendCommand(EstablishConnectionCommand.class);
        Status.getInstance().write("Started connection to server: " + ip);
    }

    /**
     * Stops listening to server
     */
    private void stopListening() {
        initMotors();

        if (client != null && client.isConnected()) {
            try {
                client.closeConnection();
            } catch (BitBotServerException ex) {
                Logger.e("Could not close the connection!");
            }
        }

        dataFactory = null;
        Status.getInstance().write("Closed connection to server");
    }

    /**
     * Send command to server
     *
     * @param id class id
     * @param param parameter as varargs
     * @throws BitBotException if something failed
     */
    private Response sendCommand(Class<?> id, String... param) throws BitBotException {
        return sendCommand(id, new ArrayList<>(Arrays.asList(param)));
    }

    private Response sendCommand(Class<?> id) throws BitBotException {
        List<String> list = new ArrayList<>();
        return sendCommand(id, list);
    }

    /**
     * Send command to server
     *
     * @param id class id
     * @param param parameter as list
     * @throws BitBotException if something failed
     */
    private Response sendCommand(Class<?> id, List<String> param) throws BitBotException {
        if (dataFactory != null) {
            CommandData data = dataFactory.getCommandData(id, param);
            return client.sendCommand(data);

        } else {
            throw new BitBotGuiException("No connection to the server");
        }
    }

    /**
     * Update motor values and send commands
     *
     * @throws BitBotException if something failed
     */
    private void updateMotors() throws BitBotException {
        int speedRight = gmRight.getCurrentValue();
        switch (statusRight) {
            case FORWARDS:
                speedRight = +speedRight;
                break;
            case BACKWARDS:
                speedRight = -speedRight;
                break;
            case STOP:
                speedRight = 0;
                break;
        }

        int speedLeft = gmLeft.getCurrentValue();
        switch (statusLeft) {
            case FORWARDS:
                speedLeft = +speedLeft;
                break;
            case BACKWARDS:
                speedLeft = -speedLeft;
                break;
            case STOP:
                speedLeft = 0;
                break;
        }

        if (client != null && client.isConnected()) {
            sendCommand(ControlMotorCommand.class, Integer.toString(speedLeft), Integer.toString(speedRight));
        }
    }

    /**
     * Saves Ip address in configuration
     *
     * @throws BitBotGuiException if the ip adress is not valid
     */
    private void saveIpAdress() throws BitBotGuiException {

        String ip = tfIpAddress.getText().trim();
        if (!Ipv4AddressHelper.validate(ip)) {
            throw new BitBotGuiException("IP Adress: " + ip + " is not valid.");
        }

        String path = Config.getDefault().get(ConfigItem.DATA_PATH.getName());
        FileCreator.writeToFile(path, ip);
        Status.getInstance().write("Saved IP Adress to configuration");
    }

    /**
     * Sets Ip adress to the textfield, but only if it is valid, reads from
     * configuration
     *
     * @throws IOException if the file could not be read
     */
    private void readIpAdress() throws IOException {

        String path = Config.getDefault().get(ConfigItem.DATA_PATH.getName());
        String ip = FileCreator.readLineFromFile(path);
        if (Ipv4AddressHelper.validate(ip)) {
            tfIpAddress.setText(ip);
            btConnectToServer.setEnabled(true);
            btDisconnect.setEnabled(false);
            btPanic.setEnabled(false);

        }
        Status.getInstance().write("Read IP Adress from configuration");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        pnTitle = new javax.swing.JPanel();
        lbTitle = new javax.swing.JLabel();
        pnBottom = new javax.swing.JPanel();
        btConnectToServer = new javax.swing.JButton();
        btStartSimulation = new javax.swing.JButton();
        tfIpAddress = new javax.swing.JTextField();
        lbIpAdress = new javax.swing.JLabel();
        btDisconnect = new javax.swing.JButton();
        btPanic = new javax.swing.JButton();
        lbStatus = new javax.swing.JLabel();
        pnMainMain = new javax.swing.JPanel();
        tpMain = new javax.swing.JTabbedPane();
        pnHomeTab = new javax.swing.JPanel();
        pnRightTacho = new javax.swing.JPanel();
        gmRight = new at.petritzdesigns.gauge.view.GaugeMeter();
        pnLeftTacho = new javax.swing.JPanel();
        gmLeft = new at.petritzdesigns.gauge.view.GaugeMeter();
        pnMain = new javax.swing.JPanel();
        btForward = new javax.swing.JButton();
        btLeft = new javax.swing.JButton();
        btBackward = new javax.swing.JButton();
        btRight = new javax.swing.JButton();
        btStop = new javax.swing.JButton();
        pnCameraSensorControl = new javax.swing.JPanel();
        tpCamera = new javax.swing.JTabbedPane();
        pnCamera = new javax.swing.JPanel();
        btCameraControlLeft = new javax.swing.JButton();
        btCameraControlDown = new javax.swing.JButton();
        btCameraControlRight = new javax.swing.JButton();
        btCameraControlUp = new javax.swing.JButton();
        btCameraControlReset = new javax.swing.JButton();
        tpSensors = new javax.swing.JTabbedPane();
        pnSensors = new javax.swing.JPanel();
        lbUltraSonicSensorLabel = new javax.swing.JLabel();
        lbUltraSonicSensorDistanceValue = new javax.swing.JLabel();
        lbUltraSonicSensorDistance = new javax.swing.JLabel();
        btUltraSonicSensorAutoRefresh = new javax.swing.JToggleButton();
        btUltraSonicSensorRefresh = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuConfiguration = new javax.swing.JMenuItem();
        spFile = new javax.swing.JPopupMenu.Separator();
        menuQuit = new javax.swing.JMenuItem();
        menuEdit = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("BitBot Control");
        setMinimumSize(new java.awt.Dimension(920, 399));

        pnTitle.setBackground(new java.awt.Color(52, 73, 94));
        pnTitle.setLayout(new java.awt.GridLayout(1, 0));

        lbTitle.setBackground(new java.awt.Color(108, 122, 137));
        lbTitle.setFont(new java.awt.Font("Lucida Grande", 0, 30)); // NOI18N
        lbTitle.setForeground(new java.awt.Color(255, 255, 255));
        lbTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTitle.setText("BitBot Control");
        lbTitle.setPreferredSize(new java.awt.Dimension(200, 70));
        lbTitle.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                onCredits(evt);
            }
        });
        pnTitle.add(lbTitle);

        getContentPane().add(pnTitle, java.awt.BorderLayout.PAGE_START);

        pnBottom.setBackground(new java.awt.Color(214, 214, 214));
        pnBottom.setLayout(new java.awt.GridBagLayout());

        btConnectToServer.setText("Connect to server");
        btConnectToServer.setFocusable(false);
        btConnectToServer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onConnectToServer(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(8, 2, 2, 2);
        pnBottom.add(btConnectToServer, gridBagConstraints);

        btStartSimulation.setText("Start Simulation");
        btStartSimulation.setFocusable(false);
        btStartSimulation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onStartSimulation(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(8, 2, 2, 2);
        pnBottom.add(btStartSimulation, gridBagConstraints);

        tfIpAddress.setColumns(10);
        tfIpAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onIpAdress(evt);
            }
        });
        tfIpAddress.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                onIpAdressKeyPressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(8, 2, 2, 2);
        pnBottom.add(tfIpAddress, gridBagConstraints);

        lbIpAdress.setText("IP Address");
        lbIpAdress.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(8, 2, 2, 2);
        pnBottom.add(lbIpAdress, gridBagConstraints);

        btDisconnect.setText("Disconnect");
        btDisconnect.setFocusable(false);
        btDisconnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onDisconnect(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(8, 2, 2, 2);
        pnBottom.add(btDisconnect, gridBagConstraints);

        btPanic.setBackground(new java.awt.Color(255, 51, 51));
        btPanic.setText("Panic");
        btPanic.setFocusable(false);
        btPanic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onPanic(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(8, 2, 2, 2);
        pnBottom.add(btPanic, gridBagConstraints);

        lbStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbStatus.setText("BitBot Control (c) 2016");
        lbStatus.setFocusable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 2, 8, 2);
        pnBottom.add(lbStatus, gridBagConstraints);

        getContentPane().add(pnBottom, java.awt.BorderLayout.PAGE_END);

        pnMainMain.setLayout(new java.awt.BorderLayout());

        pnHomeTab.setLayout(new java.awt.BorderLayout());

        gmRight.setDescription("Right Motor");
        gmRight.setFocusable(false);
        gmRight.setPadding(new java.lang.Integer(18));
        gmRight.setPreferredSize(new java.awt.Dimension(300, 200));
        pnRightTacho.add(gmRight);

        pnHomeTab.add(pnRightTacho, java.awt.BorderLayout.LINE_END);

        gmLeft.setDescription("Left Motor");
        gmLeft.setFocusable(false);
        gmLeft.setPadding(new java.lang.Integer(18));
        gmLeft.setPreferredSize(new java.awt.Dimension(300, 200));
        pnLeftTacho.add(gmLeft);

        pnHomeTab.add(pnLeftTacho, java.awt.BorderLayout.LINE_START);

        pnMain.setBackground(new java.awt.Color(237, 237, 237));
        pnMain.setFocusable(false);
        pnMain.setLayout(new java.awt.GridBagLayout());

        btForward.setText("Forward");
        btForward.setFocusable(false);
        btForward.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onForward(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnMain.add(btForward, gridBagConstraints);

        btLeft.setText("Left");
        btLeft.setFocusable(false);
        btLeft.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onLeft(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnMain.add(btLeft, gridBagConstraints);

        btBackward.setText("Backward");
        btBackward.setFocusable(false);
        btBackward.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onBackward(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnMain.add(btBackward, gridBagConstraints);

        btRight.setText("Right");
        btRight.setFocusable(false);
        btRight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onRight(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnMain.add(btRight, gridBagConstraints);

        btStop.setText("Stop");
        btStop.setFocusable(false);
        btStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onStop(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnMain.add(btStop, gridBagConstraints);

        pnHomeTab.add(pnMain, java.awt.BorderLayout.CENTER);

        tpMain.addTab("Home", pnHomeTab);

        pnMainMain.add(tpMain, java.awt.BorderLayout.NORTH);

        pnCameraSensorControl.setLayout(new java.awt.BorderLayout());

        pnCamera.setMinimumSize(new java.awt.Dimension(400, 400));
        pnCamera.setLayout(new java.awt.GridBagLayout());

        btCameraControlLeft.setText("<");
        btCameraControlLeft.setFocusable(false);
        btCameraControlLeft.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onCameraControl(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        pnCamera.add(btCameraControlLeft, gridBagConstraints);

        btCameraControlDown.setText("v");
        btCameraControlDown.setFocusable(false);
        btCameraControlDown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onCameraControl(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        pnCamera.add(btCameraControlDown, gridBagConstraints);

        btCameraControlRight.setText(">");
        btCameraControlRight.setFocusable(false);
        btCameraControlRight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onCameraControl(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        pnCamera.add(btCameraControlRight, gridBagConstraints);

        btCameraControlUp.setText("^");
        btCameraControlUp.setFocusable(false);
        btCameraControlUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onCameraControl(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        pnCamera.add(btCameraControlUp, gridBagConstraints);

        btCameraControlReset.setText("R");
        btCameraControlReset.setFocusable(false);
        btCameraControlReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onCameraControl(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        pnCamera.add(btCameraControlReset, gridBagConstraints);

        tpCamera.addTab("Camera", pnCamera);

        pnCameraSensorControl.add(tpCamera, java.awt.BorderLayout.LINE_START);

        pnSensors.setLayout(new java.awt.GridBagLayout());

        lbUltraSonicSensorLabel.setFont(new java.awt.Font("Lucida Grande", 1, 16)); // NOI18N
        lbUltraSonicSensorLabel.setText("Ultra Sonic Sensor");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        pnSensors.add(lbUltraSonicSensorLabel, gridBagConstraints);

        lbUltraSonicSensorDistanceValue.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        lbUltraSonicSensorDistanceValue.setText("- mm");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 13);
        pnSensors.add(lbUltraSonicSensorDistanceValue, gridBagConstraints);

        lbUltraSonicSensorDistance.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        lbUltraSonicSensorDistance.setText("Distance");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 11);
        pnSensors.add(lbUltraSonicSensorDistance, gridBagConstraints);

        btUltraSonicSensorAutoRefresh.setText("AR");
        btUltraSonicSensorAutoRefresh.setFocusable(false);
        btUltraSonicSensorAutoRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onUltraSonicSensorAutoRefresh(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        pnSensors.add(btUltraSonicSensorAutoRefresh, gridBagConstraints);

        btUltraSonicSensorRefresh.setText("R");
        btUltraSonicSensorRefresh.setFocusable(false);
        btUltraSonicSensorRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onUltraSonicSensorRefresh(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        pnSensors.add(btUltraSonicSensorRefresh, gridBagConstraints);

        tpSensors.addTab("Sensors", pnSensors);

        pnCameraSensorControl.add(tpSensors, java.awt.BorderLayout.CENTER);

        pnMainMain.add(pnCameraSensorControl, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnMainMain, java.awt.BorderLayout.CENTER);

        menuFile.setText("File");

        menuConfiguration.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_COMMA, java.awt.event.InputEvent.CTRL_MASK));
        menuConfiguration.setText("Configuration");
        menuConfiguration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onConfiguration(evt);
            }
        });
        menuFile.add(menuConfiguration);
        menuFile.add(spFile);

        menuQuit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        menuQuit.setText("Quit");
        menuQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onQuit(evt);
            }
        });
        menuFile.add(menuQuit);

        menuBar.add(menuFile);

        menuEdit.setText("Edit");
        menuBar.add(menuEdit);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Starts external Simulation Frame, auto connects to simulation server that
     * is started.
     *
     * @param evt action event
     */
    private void onStartSimulation(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onStartSimulation
//        try {
//            if (simulation != null) {
//                if (simulation.isVisible()) {
//                    if (simulation.isRunning()) {
//                        simulation.stopServer();
//                    }
//                }
//            } else {
//                simulation = new SimulationFrame();
//            }
//
//            simulation.setVisible(true);
//            simulation.requestFocus();
//            simulation.startServer();
//            listenToSimulator();
//
//            btConnectToServer.setEnabled(false);
//            btDisconnect.setEnabled(true);
//            btPanic.setEnabled(true);
//        } catch (Exception ex) {
//            Logger.v(this, ex);
//        }
    }//GEN-LAST:event_onStartSimulation

    /**
     * This event is called when the user clicks the title<br>
     * it should show the credits
     *
     * @param evt action event
     */
    private void onCredits(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onCredits
        JOptionPane.showMessageDialog(this, "BitBot Control was made by Markus Petritz, Jonathan Resch and Julian Maierl. (c) 2015");
    }//GEN-LAST:event_onCredits

    /**
     * This event is called when the user clicks connect to server<br>
     * it takes the ip adress and connects to the server
     *
     * @param evt action event
     */
    private void onConnectToServer(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onConnectToServer
        switch (Config.getDefault().get(ConfigItem.CONNECTION_TYPE.getName())) {
            case "tcp":
                udpMode = false;
                break;
            case "udp":
                udpMode = true;
                break;
        }

        try {
            String ip = tfIpAddress.getText().trim();
            if (!Ipv4AddressHelper.validate(ip)) {
                throw new BitBotGuiException("IP Adress " + ip + " is not valid.");
            }
            listenToServer(ip);
            saveIpAdress();

            tfIpAddress.setEnabled(false);
            btConnectToServer.setEnabled(false);
            btDisconnect.setEnabled(true);
            btPanic.setEnabled(true);
            btStartSimulation.setEnabled(false);

            tfIpAddress.setFocusable(false);
        } catch (BitBotServerException ex) {
            Logger.v(this, ex.getInitialException());
        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onConnectToServer

    /**
     * This event is called when the user clicks disconnect<br>
     * it disconnects from the server
     *
     * @param evt action event
     */
    private void onDisconnect(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onDisconnect
        try {
            stopListening();

            tfIpAddress.setEnabled(true);
            tfIpAddress.setFocusable(true);
            tfIpAddress.requestFocus();
            btConnectToServer.setEnabled(true);
            btDisconnect.setEnabled(false);
            btPanic.setEnabled(false);
            btStartSimulation.setEnabled(true);
        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onDisconnect

    /**
     * This event is called when the user clicks the panic button<br>
     * it shuts down everything completly
     *
     * @param evt action event
     */
    private void onPanic(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onPanic
        try {
            stopListening();

            tfIpAddress.setEnabled(true);
            tfIpAddress.requestFocus();
            btConnectToServer.setEnabled(true);
            btDisconnect.setEnabled(false);
            btPanic.setEnabled(false);
            btStartSimulation.setEnabled(true);
        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onPanic

    /**
     * This event is called when the user clicks quit on the menu bar<br>
     * it quits the application completely
     *
     * @param evt action event
     */
    private void onQuit(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onQuit
        if (client != null) {
            int ret = JOptionPane.showConfirmDialog(this,
                    "Der Client ist noch mit dem Server verbunden. Möchstest du die Verbindung trennen und beenden?",
                    "Client ist noch verbunden.",
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.WARNING_MESSAGE);

            switch (ret) {
                case JOptionPane.YES_OPTION:
                    stopListening();
                    System.exit(0);
                    break;
                case JOptionPane.NO_OPTION:
                    System.exit(0);
                    break;
                default:
                    break;
            }
        }
        System.exit(0);
    }//GEN-LAST:event_onQuit

    /**
     * This event is called when the user clicks configuration on the menu
     * bar<br>
     * it show the configuration dialog
     *
     * @param evt action event
     */
    private void onConfiguration(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onConfiguration
        try {
            if (configuration == null) {
                configuration = new ConfigurationDialog(this, true);
            }

            configuration.setVisible(true);
            configuration.requestFocus();
        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onConfiguration

    private void onIpAdress(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onIpAdress
        if (btConnectToServer.isEnabled()) {
            onConnectToServer(evt);
        }
    }//GEN-LAST:event_onIpAdress

    private void onIpAdressKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_onIpAdressKeyPressed
        if (!tfIpAddress.getText().trim().isEmpty()
                && Ipv4AddressHelper.validate(tfIpAddress.getText().trim())) {
            btConnectToServer.setEnabled(true);
            btDisconnect.setEnabled(false);
            btPanic.setEnabled(false);
        } else {
            btConnectToServer.setEnabled(false);
            btDisconnect.setEnabled(false);
            btPanic.setEnabled(false);
        }
    }//GEN-LAST:event_onIpAdressKeyPressed

    private void onForward(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onForward
        try {
            statusLeft = MotorStatus.FORWARDS;
            statusRight = MotorStatus.FORWARDS;
            updateMotors();
        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onForward

    private void onBackward(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onBackward
        try {
            statusLeft = MotorStatus.BACKWARDS;
            statusRight = MotorStatus.BACKWARDS;
            updateMotors();
        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onBackward

    private void onLeft(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onLeft
        try {
            statusRight = MotorStatus.STOP;
            updateMotors();
        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onLeft

    private void onRight(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onRight
        try {
            statusLeft = MotorStatus.STOP;
            updateMotors();
        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onRight

    private void onStop(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onStop
        try {
            statusLeft = MotorStatus.STOP;
            statusRight = MotorStatus.STOP;
            updateMotors();
        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onStop

    private void onCameraControl(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onCameraControl
        //TODO make servo control work
        //DEBUG only:
        try {
            if (client != null && client.isConnected()) {
                String panDegrees = JOptionPane.showInputDialog("Pan Degrees");
                String tiltDegrees = JOptionPane.showInputDialog("Tilt Degrees");
                sendCommand(ControlCameraCommand.class, panDegrees, tiltDegrees);
            }
        } catch (HeadlessException | BitBotException ex) {
            Logger.v(this, ex);
        }

//        JButton source = (JButton) evt.getSource();
//        if (source.equals(btCameraControlUp)) {
//            System.out.println("oyoooo");
//        }
//        else if(source.equals(btCameraControlDown)) {
//            
//        }
//        else if(source.equals(btCameraControlLeft)) {
//            
//        }
//        else if(source.equals(btCameraControlRight)) {
//            
//        }
//        else if(source.equals(btCameraControlReset)) {
//            
//        }
    }//GEN-LAST:event_onCameraControl

    private void onUltraSonicSensorRefresh(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onUltraSonicSensorRefresh
        try {
            if (client != null && client.isConnected()) {
                Response r = sendCommand(DistanceCommand.class);
                if (r instanceof DistanceResponse.Data) {
                    DistanceResponse.Data response = (DistanceResponse.Data) r;
                    lbUltraSonicSensorDistanceValue.setText(response.getDistance() + " mm");
                } else if (r instanceof FailureResponse.Data) {
                    System.err.println("Error");
                }
            }
        } catch (Exception e) {
            //Ignore exceptions on auto refresh... so the user is not spammed
            if (evt != null) {
                Logger.v(this, e);
            }
            Logger.w(e);
        }
    }//GEN-LAST:event_onUltraSonicSensorRefresh

    private void onUltraSonicSensorAutoRefresh(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onUltraSonicSensorAutoRefresh
        try {
            if (client == null || !client.isConnected()) {
                throw new Exception("Not connected to a server.");
            }
            if (!btUltraSonicSensorAutoRefresh.isSelected()) {
                if (ultraSonicSensorTimer != null) {
                    ultraSonicSensorTimer.cancel();
                }
            } else {
                ultraSonicSensorTimer = new Timer("Ultra Sonic Sensor Auto Refresh");
                ultraSonicSensorTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (btUltraSonicSensorAutoRefresh.isSelected()) {
                            onUltraSonicSensorRefresh(null);
                        } else {
                            this.cancel();
                        }
                    }
                }, 0, 1000);
            }
        } catch (Exception e) {
            Logger.v(this, e);
        }
    }//GEN-LAST:event_onUltraSonicSensorAutoRefresh

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btBackward;
    private javax.swing.JButton btCameraControlDown;
    private javax.swing.JButton btCameraControlLeft;
    private javax.swing.JButton btCameraControlReset;
    private javax.swing.JButton btCameraControlRight;
    private javax.swing.JButton btCameraControlUp;
    private javax.swing.JButton btConnectToServer;
    private javax.swing.JButton btDisconnect;
    private javax.swing.JButton btForward;
    private javax.swing.JButton btLeft;
    private javax.swing.JButton btPanic;
    private javax.swing.JButton btRight;
    private javax.swing.JButton btStartSimulation;
    private javax.swing.JButton btStop;
    private javax.swing.JToggleButton btUltraSonicSensorAutoRefresh;
    private javax.swing.JButton btUltraSonicSensorRefresh;
    private at.petritzdesigns.gauge.view.GaugeMeter gmLeft;
    private at.petritzdesigns.gauge.view.GaugeMeter gmRight;
    private javax.swing.JLabel lbIpAdress;
    private javax.swing.JLabel lbStatus;
    private javax.swing.JLabel lbTitle;
    private javax.swing.JLabel lbUltraSonicSensorDistance;
    private javax.swing.JLabel lbUltraSonicSensorDistanceValue;
    private javax.swing.JLabel lbUltraSonicSensorLabel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuConfiguration;
    private javax.swing.JMenu menuEdit;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenuItem menuQuit;
    private javax.swing.JPanel pnBottom;
    private javax.swing.JPanel pnCamera;
    private javax.swing.JPanel pnCameraSensorControl;
    private javax.swing.JPanel pnHomeTab;
    private javax.swing.JPanel pnLeftTacho;
    private javax.swing.JPanel pnMain;
    private javax.swing.JPanel pnMainMain;
    private javax.swing.JPanel pnRightTacho;
    private javax.swing.JPanel pnSensors;
    private javax.swing.JPanel pnTitle;
    private javax.swing.JPopupMenu.Separator spFile;
    private javax.swing.JTextField tfIpAddress;
    private javax.swing.JTabbedPane tpCamera;
    private javax.swing.JTabbedPane tpMain;
    private javax.swing.JTabbedPane tpSensors;
    // End of variables declaration//GEN-END:variables

}
